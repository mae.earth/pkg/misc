/* mae.earth/pkg/misc/sleeve/sleeve_test.go */
package sleeve

import (
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"sort"
	"strings"
	"sync"
	"time"

	"github.com/minio/blake2b-simd" /* generally faster than sha1 and also has a size configuration */
)

const (
	CurrentInternalVersion int = 0
)

const (
	Magic   = byte('S')
	Version = byte(0)

	ChecksumLength int    = 8
	TagDelimiter   string = ";"
)

var (
	ErrBadMagic       = errors.New("bad magic")
	ErrBadVersion     = errors.New("bad version")
	ErrBadChecksum    = errors.New("bad checksum")
	ErrBadContentType = errors.New("bad content type")
)

var bufPool = sync.Pool{New: func() interface{} { return new(bytes.Buffer) }}

type KeyValuePairs map[string]string

/* Set */
func (kv KeyValuePairs) Set(tag string, values ...string) KeyValuePairs {

	if len(tag) == 0 {
		return kv
	}

	kv[tag] = strings.Join(values, TagDelimiter)

	return kv
}

/* Get */
func (kv KeyValuePairs) Get(tag string) []string {
	if len(tag) == 0 || len(kv) == 0 {
		return nil
	}

	if v, ok := kv[tag]; ok {
		out := strings.Split(v, TagDelimiter)
		sort.Slice(out, func(i, j int) bool { return out[i] < out[j] })
		return out
	}
	return nil
}

/* GetFirst */
func (kv KeyValuePairs) GetFirst(tag, def string) string {
	if len(tag) == 0 || len(kv) == 0 {
		return def
	}

	if v, ok := kv[tag]; ok {
		out := strings.Split(v, TagDelimiter)
		return out[0]
	}
	return def
}

/* Remove */
func (kv KeyValuePairs) Remove(tags ...string) KeyValuePairs {
	if len(tags) == 0 || len(kv) == 0 {
		return kv
	}

	for _, tag := range tags {
		if _, ok := kv[tag]; ok {

			delete(kv, tag)
		}
	}

	return kv
}

/* Append */
func (kv KeyValuePairs) Append(tag string, values ...string) KeyValuePairs {
	if len(tag) == 0 || len(values) == 0 || len(kv) == 0 {
		return nil
	}

	if v, ok := kv[tag]; ok {

		out := strings.Split(v, TagDelimiter)
		/* check for duplicates */
		for _, val := range values {
			found := false
			for _, o := range out {
				if o == val {
					found = true
					break
				}
			}
			if !found {
				out = append(out, val)
			}
		}

		kv[tag] = strings.Join(out, TagDelimiter)
	}
	return kv
}

/* Size  -- TODO: this is not required, because len(KeyValuePairs) works just fine */
func (kv KeyValuePairs) Size() int {
	return len(kv)
}

/* Sleeve */
type Sleeve struct {
	Tags       KeyValuePairs `json:"T" msgpack:"T"`
	UTags      KeyValuePairs `json:"UT" msgpack:"UT"`
	Attributes KeyValuePairs `json:"A" msgpack:"A"` /* for ABAC */

	URI                string `json:"uri,omitempty" msgpack:"uri,omitempty"`
	ContentType        string `json:"-" msgpack:"-"`
	ContentDisposition string `json:"cd,omitempty" msgpack:"cd,omitempty"`

	Created  time.Time `json:"c,omitempty" msgpack:"c,omitempty"`
	Modified time.Time `json:"m,omitempty" msgpack:"m,omitempty"`

	Version         int `json:"v,omitempty" msgpack:"v,omitempty"`
	InternalVersion int `json:"iv,omitempty" msgpack:"iv,omitempty"`

	Checksum string `json:"-" msgpack:"-"`
	Content  []byte `json:"-" msgpack:"-"`
}

/* T shortcut to ContentType, because lazy */
func (sleeve *Sleeve) T() string { return sleeve.ContentType }

/* D shortcut to ContentDisposition (because above) */
func (sleeve *Sleeve) D() string { return sleeve.ContentDisposition }

/* Encode */
func (sleeve *Sleeve) Encode() ([]byte, error) {

	/* For simple content lookups, rather than having to decode the entire object
		 * just to get content, we place the content and the content type at the end
	   * of the data and provide an integer at the beginning to skip the encoded
	   * sleeve data. This allows us to get the content-type and actual content
	   * without having to decode the sleeve.
	   *
	   * The content, the first N (as defined by ChecksumLength) bytes are the
	   * checksum. This allows us to check the content without having to decode
	   * the sleeve.
	*/

	msgcontent, err := json.Marshal(sleeve)
	if err != nil {
		return nil, err
	}

	l := uint64(len(msgcontent))

	buf := bufPool.Get().(*bytes.Buffer)
	buf.Reset()
	defer bufPool.Put(buf)

	//buf := bytes.NewBuffer(nil)
	buf.WriteByte(Magic)
	buf.WriteByte(Version)

	if err := binary.Write(buf, binary.LittleEndian, l); err != nil {
		return nil, err
	}

	buf.Write(msgcontent)

	buf.WriteByte(byte(len(sleeve.ContentType)))
	buf.Write([]byte(sleeve.ContentType))

	buf.Write([]byte(sleeve.Checksum))
	buf.Write(sleeve.Content)

	return buf.Bytes(), nil
}

/* ToSleeve */
func ToSleeve(content []byte) (*Sleeve, error) {

	var l uint64
	//buf := bytes.NewBuffer(content)
	buf := bufPool.Get().(*bytes.Buffer)
	defer bufPool.Put(buf)
	buf.Reset()
	buf.Write(content)
	magic, err := buf.ReadByte()
	if err != nil {
		return nil, ErrBadMagic
	}

	if magic != Magic {
		return nil, ErrBadMagic
	}

	version, err := buf.ReadByte()
	if err != nil {
		return nil, ErrBadVersion
	}

	if int(version) > int(Version) {
		return nil, ErrBadVersion
	}

	if err := binary.Read(buf, binary.LittleEndian, &l); err != nil {
		return nil, err
	}

	var sleeve *Sleeve
	if err := json.Unmarshal(buf.Next(int(l)), &sleeve); err != nil {
		return nil, err
	}

	ctl, err := buf.ReadByte()
	if err != nil {
		return nil, ErrBadContentType
	}

	ct := buf.Next(int(ctl))
	sleeve.ContentType = string(ct)

	ck := buf.Next(ChecksumLength)
	if len(ck) != ChecksumLength {
		return nil, ErrBadChecksum
	}

	sleeve.Checksum = string(ck)

	con := buf.Bytes()
	if len(con) > 0 {
		sleeve.Content = make([]byte, len(con))
		copy(sleeve.Content, con)

		/* do a checksum check */
		b, _ := blake2b.New(&blake2b.Config{Size: uint8(ChecksumLength)})
		b.Write(sleeve.Content)
		if string(b.Sum(nil)) != string(ck) {
			return sleeve, ErrBadChecksum
		}
	}

	return sleeve, nil
}

/* GetContent returns ContentType and Content */
func GetContent(content []byte) (string, []byte, error) {

	var l uint64
	//buf := bytes.NewBuffer(content)
	buf := bufPool.Get().(*bytes.Buffer)
	defer bufPool.Put(buf)
	buf.Reset()
	buf.Write(content)

	magic, err := buf.ReadByte()
	if err != nil {
		return "", nil, ErrBadMagic
	}

	if magic != Magic {
		return "", nil, ErrBadMagic
	}

	version, err := buf.ReadByte()
	if err != nil {
		return "", nil, ErrBadVersion
	}

	if int(version) > int(Version) {
		return "", nil, ErrBadVersion
	}

	if err := binary.Read(buf, binary.LittleEndian, &l); err != nil {
		return "", nil, err
	}

	/* seek forward, skipping the encoded sleeve */
	buf.Next(int(l))

	ctl, err := buf.ReadByte()
	if err != nil {
		return "", nil, ErrBadContentType
	}

	ct := buf.Next(int(ctl))

	ck := buf.Next(ChecksumLength)
	if len(ck) != ChecksumLength {
		return "", nil, ErrBadChecksum
	}

	con := buf.Bytes()

	b, _ := blake2b.New(&blake2b.Config{Size: uint8(ChecksumLength)})
	b.Write(con)

	if string(ck) != string(b.Sum(nil)) {
		return string(ct), con, ErrBadChecksum
	}

	return string(ct), con, nil
}

/* GetContentWithoutChecksum */
func GetContentWithoutChecksum(content []byte) (string, []byte, error) {

	var l uint64
	//buf := bytes.NewBuffer(content)
	buf := bufPool.Get().(*bytes.Buffer)
	defer bufPool.Put(buf)
	buf.Reset()
	buf.Write(content)

	magic, err := buf.ReadByte()
	if err != nil {
		return "", nil, ErrBadMagic
	}

	if magic != Magic {
		return "", nil, ErrBadMagic
	}

	version, err := buf.ReadByte()
	if err != nil {
		return "", nil, ErrBadVersion
	}

	if int(version) > int(Version) {
		return "", nil, ErrBadVersion
	}

	if err := binary.Read(buf, binary.LittleEndian, &l); err != nil {
		return "", nil, err
	}

	/* seek forward, skipping the encoded sleeve */
	buf.Next(int(l))

	ctl, err := buf.ReadByte()
	if err != nil {
		return "", nil, ErrBadContentType
	}

	ct := buf.Next(int(ctl))

	buf.Next(ChecksumLength)

	return string(ct), buf.Bytes(), nil
}

/* GetContentSize */
func GetContentSize(content []byte) (int, error) {

	var l uint64

	buf := bufPool.Get().(*bytes.Buffer)
	defer bufPool.Put(buf)
	buf.Reset()
	buf.Write(content)

	magic, err := buf.ReadByte()
	if err != nil {
		return 0, ErrBadMagic
	}

	if magic != Magic {
		return 0, ErrBadMagic
	}

	version, err := buf.ReadByte()
	if err != nil {
		return 0, ErrBadVersion
	}

	if int(version) > int(Version) {
		return 0, ErrBadVersion
	}
	if err := binary.Read(buf, binary.LittleEndian, &l); err != nil {
		return 0, err
	}

	buf.Next(int(l))
	ctl, err := buf.ReadByte()
	if err != nil {
		return 0, ErrBadContentType
	}

	buf.Next(int(ctl) + ChecksumLength)

	return buf.Len(), nil
}

/* DotFromContent returns dot label, assumes Mrecord */
func DotFromContent(content []byte) (string, error) {

	var l uint64
	buf := bufPool.Get().(*bytes.Buffer)
	defer bufPool.Put(buf)
	buf.Reset()
	buf.Write(content)

	magic, err := buf.ReadByte()
	if err != nil {
		return "", err
	}

	if magic != Magic {
		return "", ErrBadMagic
	}

	version, err := buf.ReadByte()
	if err != nil {
		return "", ErrBadVersion
	}

	if int(version) > int(Version) {
		return "", ErrBadVersion
	}

	if err := binary.Read(buf, binary.LittleEndian, &l); err != nil {
		return "", err
	}

	buf.Next(int(l))

	ctl, err := buf.ReadByte()
	if err != nil {
		return "", err
	}

	ct := buf.Next(int(ctl))

	buf.Next(ChecksumLength)

	return fmt.Sprintf("{%s | content is %dbytes}", string(ct), buf.Len()), nil
}

/* Dot -- t is type Light | Heavy whilst output is Dot Shape (Circle,MRecord) and Label */
func (sleeve *Sleeve) Dot(t string) (string, string) {

	pural := func(m string, n int) string {
		if n == 0 || n > 1 {
			return m + "s"
		}
		return m
	}

	if strings.ToLower(t) == "heavy" || strings.ToLower(t) == "very heavy" {

		label := fmt.Sprintf("{%s; %s | ver %d.%d}", sleeve.ContentType, sleeve.ContentDisposition,
			sleeve.InternalVersion, sleeve.Version)

		label += fmt.Sprintf("|{ created %s }", sleeve.Created)

		if len(sleeve.URI) > 0 {
			label += fmt.Sprintf("|{ uri %s }", sleeve.URI)
		}

		if strings.ToLower(t) == "very heavy" {

			attributes := make([]string, 0)
			for k, v := range sleeve.Attributes {
				attributes = append(attributes, fmt.Sprintf("%s = %s", k, v))
			}

			sort.Slice(attributes, func(i, j int) bool { return attributes[i] < attributes[j] })

			tags := make([]string, 0)
			for k, v := range sleeve.Tags {
				tags = append(tags, fmt.Sprintf("%s = %s", k, v))
			}

			sort.Slice(tags, func(i, j int) bool { return tags[i] < tags[j] })

			utags := make([]string, 0)
			for k, v := range sleeve.UTags {
				utags = append(utags, fmt.Sprintf("%s = %s", k, v))
			}

			sort.Slice(utags, func(i, j int) bool { return utags[i] < utags[j] })

			tag := ""
			for _, v := range attributes {
				tag += fmt.Sprintf("|{ attribute %s }", v)
			}

			for _, v := range tags {
				tag += fmt.Sprintf("|{ tag %s }", v)
			}

			for _, v := range utags {
				tag += fmt.Sprintf("|{ user tag %s }", v)
			}

			label += tag

		} else {

			tag := fmt.Sprintf("%d %s", len(sleeve.Attributes), pural("attribute", len(sleeve.Attributes)))
			tag += fmt.Sprintf(" | %d %s", len(sleeve.Tags), pural("tag", len(sleeve.Tags)))
			tag += fmt.Sprintf(" | %d user %s", len(sleeve.UTags), pural("tag", len(sleeve.UTags)))

			label += fmt.Sprintf("|{%s}", tag)
		}

		label += fmt.Sprintf("|{%d bytes | %s}", len(sleeve.Content), hex.EncodeToString([]byte(sleeve.Checksum)))

		return "Mrecord", fmt.Sprintf("{%s}", label)
	}

	return "circle", fmt.Sprintf("%s", sleeve.ContentType)
}

/* Copy */
func (sleeve *Sleeve) Copy() *Sleeve {

	nsleeve := New(sleeve.ContentType, sleeve.ContentDisposition, sleeve.Content)
	nsleeve.Created = sleeve.Created
	nsleeve.Modified = sleeve.Modified
	nsleeve.Version = sleeve.Version
	nsleeve.URI = sleeve.URI

	for k, v := range sleeve.Attributes {
		nsleeve.Attributes[k] = v
	}

	for k, v := range sleeve.Tags {
		nsleeve.Tags[k] = v
	}

	for k, v := range sleeve.UTags {
		nsleeve.UTags[k] = v
	}

	return nsleeve
}

/* CopyWithContent */
func (sleeve *Sleeve) CopyWithContent(content []byte) *Sleeve {

	nsleeve := New(sleeve.ContentType, sleeve.ContentDisposition, content)
	nsleeve.Created = sleeve.Created
	nsleeve.Modified = sleeve.Modified
	nsleeve.Version = sleeve.Version + 1 /* because we assume the content is different */

	for k, v := range sleeve.Attributes {
		nsleeve.Attributes[k] = v
	}

	for k, v := range sleeve.Tags {
		nsleeve.Tags[k] = v
	}

	for k, v := range sleeve.UTags {
		nsleeve.UTags[k] = v
	}
	return nsleeve
}

/* New */
func New(contenttype, contentdispo string, content []byte) *Sleeve {

	now := time.Now()

	/* we encode contenttype and contentdispo as a byte length so we must do something about it here */
	if len(contenttype) > 256 {
		contenttype = contenttype[:255]
	}

	if len(contentdispo) > 256 {
		contentdispo = contentdispo[:255]
	}

	sleeve := &Sleeve{ContentType: contenttype, ContentDisposition: contentdispo, Created: now, Modified: now,
		Version: 0, InternalVersion: CurrentInternalVersion,
		Tags:       KeyValuePairs(make(map[string]string, 0)),
		UTags:      KeyValuePairs(make(map[string]string, 0)),
		Attributes: KeyValuePairs(make(map[string]string, 0))}

	if len(content) > 0 {
		sleeve.Content = make([]byte, len(content))
		copy(sleeve.Content, content)
	}

	b, _ := blake2b.New(&blake2b.Config{Size: uint8(ChecksumLength)})
	b.Write(content)
	sleeve.Checksum = string(b.Sum(nil))

	return sleeve
}
