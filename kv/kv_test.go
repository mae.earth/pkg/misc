/* mae.earth/pkg/misc/kv/kv_test.go */
package kv

import (
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func Test_KV(t *testing.T) {
	Convey("KV", t, func() {
		Convey("simple", func() {
			s := New()
			So(s, ShouldNotBeNil)

			Convey("set foo as []string", func() {

				So(s.Set("foo", "bar"), ShouldBeTrue)

				Convey("get foo", func() {

					str, ok := s.Get("foo")
					So(str, ShouldNotBeEmpty)
					So(str[0], ShouldEqual, "bar")
					So(ok, ShouldBeTrue)
				})
			})

			Convey("set foo as string", func() {

				So(s.Set("foo", "hello there"), ShouldBeTrue)

				Convey("get foo as string", func() {

					So(s.String("foo", "-"), ShouldEqual, "hello there")
				})

				Convey("long form", func() {

					So(s.Set("foo", "bar", int64(1), 2.345, 0.45, "bye", 0.0, false), ShouldBeTrue)

					So(s.String("foo", "-"), ShouldEqual, "bye")
				})

			})

			Convey("set foo as int64", func() {

				So(s.Set("foo", int64(101)), ShouldBeTrue)

				Convey("get foo as int64", func() {

					So(s.Int64("foo", 0), ShouldEqual, 101)
				})
			})

			Convey("set foo as float64", func() {

				So(s.Set("foo", float64(1.2345)), ShouldBeTrue)

				Convey("get foo as float64", func() {
					So(s.Float64("foo", 0.0), ShouldEqual, 1.2345)
				})
			})

			Convey("set foo as bool", func() {

				So(s.Set("foo", true), ShouldBeTrue)

				Convey("get foo as bool", func() {
					So(s.Bool("foo", false), ShouldBeTrue)
				})
			})

		})

		Convey("example with hash", func() {

			s := New()
			So(s.Set("foo", "bar", int64(1), 2.43, 0.3, "hello", false), ShouldBeTrue)
			So(s.Size(), ShouldEqual, 1)
			So(s.Hash(), ShouldEqual, `9e3c209f524fd80ab354b719defff9f40273043ea0be875a831c86fad63e0a9e49765e8669f271a5315efa18c2434c05e54fedbbdab7c92091a18b8ade961431`)

		})

	})
}

func Benchmark_String(b *testing.B) {

	b.StopTimer()
	store := New()
	store.Set("foo", "bar", int64(1), 2.345, 0.45, "bye", 0.0)
	b.StartTimer()

	for i := 0; i < b.N; i++ {

		store.String("foo", "-")
	}
}
