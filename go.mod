module mae.earth/pkg/misc

go 1.14

require (
	github.com/minio/blake2b-simd v0.0.0-20160723061019-3f5f724cb5b1
	github.com/smartystreets/goconvey v1.6.4
	github.com/vmihailenco/msgpack v4.0.4+incompatible
	mae.earth/pkg/card v0.0.0-20200317092604-68688628bb73
)
