package scrabble

import (
	"sort"
	"strings"
	"sync"
)

var internal struct {
	sync.RWMutex
	letters map[rune]int
}

func init() {
	internal.Lock()
	defer internal.Unlock()

	internal.letters = make(map[rune]int, 0)

	internal.letters['A'] = 1
	internal.letters['B'] = 3
	internal.letters['C'] = 3
	internal.letters['D'] = 2

	internal.letters['E'] = 1
	internal.letters['F'] = 4
	internal.letters['G'] = 2
	internal.letters['H'] = 4

	internal.letters['I'] = 1
	internal.letters['J'] = 8
	internal.letters['K'] = 5
	internal.letters['L'] = 1

	internal.letters['M'] = 3
	internal.letters['N'] = 1
	internal.letters['O'] = 1
	internal.letters['P'] = 3

	internal.letters['Q'] = 10
	internal.letters['R'] = 1
	internal.letters['S'] = 1
	internal.letters['T'] = 1

	internal.letters['U'] = 1
	internal.letters['V'] = 4
	internal.letters['W'] = 4
	internal.letters['X'] = 8

	internal.letters['Y'] = 4
	internal.letters['Z'] = 10
}

func Score(word string) int {
	internal.RLock()
	defer internal.RUnlock()

	score := 0

	for _, c := range strings.ToUpper(word) {

		if v, exists := internal.letters[c]; exists {
			score += v
		}
	}
	return score
}

func ScoreAtLimit(word string) int {
	/* max limit is 7 letters */
	if len(word) <= 7 {
		return Score(word)
	}

	internal.RLock()
	defer internal.RUnlock()

	l := make([]int, 0)

	for _, c := range strings.ToUpper(word) {
		if v, exists := internal.letters[c]; exists {
			l = append(l, v)
		}
	}

	sort.Slice(l, func(i, j int) bool { return l[i] > l[j] })

	score := 0
	for i := 0; i < 7; i++ {
		score += l[i]
	}

	return score
}
