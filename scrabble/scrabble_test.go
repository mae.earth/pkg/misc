package scrabble

import (
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func Test_Scrabble(t *testing.T) {
	Convey("Scrabble", t, func() {

		So(Score("abac"), ShouldEqual, 8)
		So(Score("zoo"), ShouldEqual, 12)

	})
}
